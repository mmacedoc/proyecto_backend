var express = require('express');

var usuarioservice = require('../service/usuario.service');
var authservice = require('../service/auth.service');


var router = express.Router();

var respuesta = {
  error: false,
  codigo: 200,
  mensaje: ''
 };

/* GET users listing. */
router.post('/login', function(req, res, next) {
  console.log(req.body);
  authservice.login(req.body.tipodocumento, req.body.numerodocumento, 
    req.body.clave, exitoOperacion, falloOperacion, res);
});


/*Crear nuevo usuario*/
router.post('/', function(req, res, next) {
  
  usuarioservice.crear(req.body, exitoOperacion, falloOperacion, res);
});

/*actualizaNuevoUsuario*/
router.put('/', function(req, res, next) {
  console.log(req.headers.token);
  authservice.validateToken(req.headers.token, 
    (body)=>{
      usuarioservice.actualizar(req.body, req.headers.token, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    })  
});


function exitoOperacion(body, response){
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: JSON.stringify(body)
  };
  response.status(200);
  console.log(body);
  response.send(respuesta);
}

function falloOperacion(error, response){
  respuesta = {
    error: true,
    codigo: 503,
    mensaje: JSON.stringify(error)
  };
  response.status(409);
  response.send(respuesta);
}

module.exports = router;
