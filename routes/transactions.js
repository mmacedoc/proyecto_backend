var express = require('express');
var authservice = require('../service/auth.service');
var transactionservice = require('../service/transaction.service');


var router = express.Router();

var respuesta = {
  error: false,
  codigo: 200,
  mensaje: ''
 };

/*Crear nueva cuenta*/
router.post('/', function(req, res, next) {
  console.log(req.body);
  
  authservice.validateToken(req.headers.token, 
    (body)=>{
      transactionservice.crear(req.body, body, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    });
});

router.get('/:idaccount', function(req, res, next) {
  console.log(req.headers);
  authservice.validateToken(req.headers.token, 
    (body)=>{
      transactionservice.obtenertxporcuenta(req.params.idaccount, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    })  
});



function exitoOperacion(body, response){
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: JSON.stringify(body)
  };
  response.status(200);
  response.send(respuesta);
}

function falloOperacion(error, response){
  respuesta = {
    error: true,
    codigo: 503,
    mensaje: JSON.stringify(error)
  };
  response.status(409);
  response.send(respuesta);
}

module.exports = router;
