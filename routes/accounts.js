var express = require('express');

var accountservice = require('../service/account.service');
var authservice = require('../service/auth.service');


var router = express.Router();

var respuesta = {
  error: false,
  codigo: 200,
  mensaje: ''
 };

/*Crear nueva cuenta*/
router.post('/', function(req, res, next) {
  console.log(req.body);
  
  authservice.validateToken(req.headers.token, 
    (body)=>{
      accountservice.crear(req.body.nombrecuenta, req.headers.token, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    });
});

router.get('/:idcliente', function(req, res, next) {
  console.log(req.headers);
  authservice.validateToken(req.headers.token, 
    (body)=>{
      accountservice.obtenerCuentasPorCliente(req.params.idcliente, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    })  
});


/*actualizacuenta*/
router.put('/', function(req, res, next) {
  console.log(req.headers.token);
  authservice.validateToken(req.headers.token, 
    (body)=>{
      accountservice.actualizar(req.body, req.headers.token, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    })  
});

router.post('/enviartoken', function(req, res, next) {
  
  authservice.validateToken(req.headers.token, 
    (body)=>{
      authservice.enviarTokenTransaccion(req.headers.token, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    });
});

router.post('/enviartokentrc', function(req, res, next) {
  
  console.log(req.body);
  authservice.validateToken(req.headers.token, 
    (body)=>{
      authservice.enviarTokenTransaccionTrc(req.body, body, exitoOperacion, falloOperacion, res);
    }, (error)=>{
      falloOperacion(error, res);
    });
});

function exitoOperacion(body, response){
  respuesta = {
    error: false,
    codigo: 200,
    mensaje: JSON.stringify(body)
  };
  response.status(200);
  response.send(respuesta);
}

function falloOperacion(error, response){
  respuesta = {
    error: true,
    codigo: 503,
    mensaje: JSON.stringify(error)
  };
  response.status(409);
  response.send(respuesta);
}

module.exports = router;
