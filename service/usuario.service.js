var request = require('request-json');
var Cryptr = require('cryptr');
var globals = require('../util/globals');
var notificacionservice = require('./notificacion.service');
var accountservice = require('./account.service');

const usuariourl = globals.urlusuarios;
const apiKey = globals.apiKey;
var usuarioservice = {
    crear: function(usuario, okcallback, errorcallback, response) {
        var objquery = {
            "tipodocumento": usuario.tipodocumento,
            "numerodocumento": usuario.numerodocumento
        }
        
        var client = request.createClient(usuariourl + '?q='+ JSON.stringify(objquery) + '&' + apiKey);
        client.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            console.log("revisando el body");
            console.log(body);
            if(body[0]){
                console.log(body[0]);
                errorcallback("Ya existe un usuario con el mismo tipo y numero de documento", response)
            }else{
                var cryptr = new Cryptr(globals.semillaencriptado);
                usuario.clave = cryptr.encrypt(usuario.clave);
                
                var client = request.createClient(usuariourl + '?' +apiKey);
                client.post('', usuario, function(err, res, body) {
                    if (err) {if(errorcallback) errorcallback(err, response)};
                    if(okcallback) {
                        accountservice.crear("Cuenta Fácil",body._id.$oid);
                        notificacionservice.enviarEmailBienvenida(usuario.correo, usuario.persona.nombre + ' ' + usuario.persona.apellido);
                        okcallback(body, response);
                    }
                });
            }
        });
    },

    actualizar: function(usuario, token, okcallback, errorcallback, response) {
        var objupdate = {};
        objupdate.$set = usuario;

        console.log(objupdate);
        var client = request.createClient(usuariourl + '/' + token + '?' + apiKey);
        client.put('', objupdate, function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(okcallback) okcallback(body, response);
        });
    }
    
}

module.exports = usuarioservice;