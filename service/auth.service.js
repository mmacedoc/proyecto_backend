var request = require('request-json');
var Cryptr = require('cryptr');
var globals = require('../util/globals');
var notificacionservice = require('./notificacion.service');

const usuariourl = globals.urlusuarios;
const urlaccounts = globals.urlaccounts;
const apiKey = globals.apiKey;
var authservice = {
    login: function(tipodocumento, nrodocumento, password, okcallback, errorcallback, response) {
        var cryptr = new Cryptr(globals.semillaencriptado);
        var objquery = {
            "tipodocumento": tipodocumento,
            "numerodocumento": nrodocumento
        }
        
        var client = request.createClient(usuariourl + '?q='+ JSON.stringify(objquery) + '&' + apiKey);
        client.get('', function(err, res, body) {
            //console.log(body);
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(okcallback) {
                if(body[0] && cryptr.decrypt(body[0].clave) == password){
                    //console.log(body[0]);
                    body[0].token = body[0]._id.$oid;
                    okcallback(body[0], response);
                }else{
                    errorcallback("Usuario y/o Clave incorrectas", response)
                }                
            }
        });
    },
    validateToken: function(token, okcallback, errorcallback) {
        var client = request.createClient(usuariourl + '/'+token + '?' + apiKey);
        client.get('', function(err, res, body) {
            //console.log(body);
            if (err) {if(errorcallback) errorcallback(err)};
            if(okcallback){
                if(body._id) okcallback(body);
                else {if(errorcallback) errorcallback("Token no econtrado")};
            }
        });
    },

    enviarTokenTransaccion: function(token, okcallback, errorcallback, response) {
        var client = request.createClient(usuariourl + '/'+token + '?' + apiKey);
        client.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err)};
            if(okcallback){
                if(body._id){
                    var tkOperacion = generateRandom();
                    console.log("tkOperacion " + tkOperacion);
                    notificacionservice.enviarTokenTransaccion( body.correo, "" + tkOperacion);
                    okcallback({"tkOperacion": tkOperacion}, response);
                } 
                else {if(errorcallback) errorcallback("Token no econtrado")};
            }
        });
    },

    enviarTokenTransaccionTrc: function(cuenta, usuario, okcallback, errorcallback, response) {

        var client = request.createClient(urlaccounts + '?q={"identificador":'+cuenta.ctadestino+'}&' + apiKey);
        client.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err)};
            if(okcallback){
                
                if(body[0]){
                    var clientc = request.createClient(usuariourl + '/'+body[0].cliente + '?' + apiKey);
                    clientc.get('', function(err, res, bodyc) {
                        var tkOperacion = generateRandom();
                        console.log("tkOperacion " + tkOperacion);
                        notificacionservice.enviarTokenTransaccion( usuario.telefono, "Estimado Cliente su token para autorizar su operacion es: " + tkOperacion);
                        okcallback({"tkOperacion": tkOperacion,
                            "cuentadestino":body[0],
                            "beneficiario": bodyc.persona.nombre + ' ' + bodyc.persona.apellido}, 
                            response);                        
                    });                    
                }else{
                    okcallback({"tkOperacion": "",
                            "cuentadestino":"",
                            "beneficiario": ""}, 
                            response);
                }
            }else{

            okcallback({"tkOperacion": "",
                            "cuentadestino":"",
                            "beneficiario": ""}, 
                            response);
            }
        });
    }
    
}

function generateRandom() {
    return Math.floor((Math.random() * 1000000) + 1);
}

module.exports = authservice;