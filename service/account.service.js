var request = require('request-json');
var globals = require('../util/globals');
var dateFormat = require('dateformat');

const accounturl = globals.urlaccounts;
const apiKey = globals.apiKey;
var accountservice = {

    crear: function(nombre, token, okcallback, errorcallback, response) {
        var accountapi = request.createClient(accounturl + '?s={"identificador":-1}&l=1&' +apiKey);
        var account = new Object();
        accountapi.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(body[0]){
                account.identificador = body[0].identificador + 1;
            }else{
                account.identificador = 1001;
            }
            account.numero = generarNroCuenta(1);
            account.nombre = nombre;
            account.saldo = 45000.00;
            account.moneda = "S/.";
            account.cliente = token;
            var accountapic = request.createClient(accounturl + '?' +apiKey);
            accountapic.post('', account, function(err, res, body) {            
                if (err) {if(errorcallback) errorcallback(err, response)};
                if(okcallback) okcallback(body, response);            
            });
        });
    },

    actualizar: function(usuario, token, okcallback, errorcallback, response) {
        var objupdate = {};
        objupdate.$set = usuario;

        console.log(objupdate);
        var client = request.createClient(usuariourl + '/' + token + '?' + apiKey);
        client.put('', objupdate, function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(okcallback) okcallback(body, response);
        });
    },

    obtenerCuentasPorCliente: function(token, okcallback, errorcallback, response) {
        var accountapi = request.createClient(accounturl + '?q={"cliente":"'+ token +'"}&' +apiKey);

        accountapi.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(okcallback) okcallback(body, response);
        });
    },

    actualizasaldo: function(monto, idcuenta) {
        var objupdate = {};
        var client = request.createClient(accounturl + '/'+idcuenta + '?' + apiKey);
        client.get('', function(err, res, body) {
            var clientupt = request.createClient(accounturl + '/' + idcuenta + '?' + apiKey);

            objupdate.$set = {"saldo":Number(Number(body.saldo) + Number(monto))};
            clientupt.put('', objupdate, function(err, res, body) {                
            });
        });        
    },

}

function generarNroCuenta(tipo){
    var numero = ""
    if(tipo=='1'){
        numero += "0011"
    }else if(tipo=='2'){
        numero += "0012"
    }else{
        numero += "0019"
    }
    numero += "0199" + dateFormat(new Date(), "yyyymm")

    return numero;
}

module.exports = accountservice;