var request = require('request-json');
var globals = require('../util/globals');
var dateFormat = require('dateformat');
var notificacionservice = require('./notificacion.service');
var accountservice = require('./account.service');

const urltransactions = globals.urltransactions;
const apiKey = globals.apiKey;
var transactionservice = {

    crear: function(transaction, usuario, okcallback, errorcallback, response) {
        
        var transactionapi = request.createClient(urltransactions + '?s={"identificador":-1}&l=1&' +apiKey);
        
        transactionapi.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(body[0]){
                transaction.identificador = body[0].identificador + 1;
            }else{
                transaction.identificador = 101001;
            }
            transaction.monto = -1*transaction.monto;
            transaction.idcontable = transaction.origen.identificador;
            transaction.fecha = dateFormat(new Date(), "yyyy/mm/dd h:MM:ss TT")
            var transactionapic = request.createClient(urltransactions + '?' +apiKey);
            transactionapic.post('', transaction, function(err, res, body) {            
                if (err) {if(errorcallback) errorcallback(err, response)};
                if(okcallback) {
                    notificacionservice.enviarEmailResumen(usuario.correo, transaction);
                    accountservice.actualizasaldo(transaction.monto, transaction.origen._id.$oid);
                    accountservice.actualizasaldo(-1*transaction.monto, transaction.destino._id.$oid);
                    var transactionDebe = transaction;
                    transactionDebe.origen = transaction.destino;
                    transactionDebe.destino = transaction.origen;
                    transactionDebe.monto = -1*transaction.monto;
                    transactionDebe.idcontable = transaction.destino.identificador;
                    transactionapic.post('', transactionDebe, function(err, res, body) { })
                    okcallback(body, response);
                }
            });
        });
    },

    obtenertxporcuenta: function(idcuenta, okcallback, errorcallback, response) {
        var accountapi = request.createClient(urltransactions + '?q={"idcontable":'+ idcuenta +'}&' +apiKey);

        accountapi.get('', function(err, res, body) {
            if (err) {if(errorcallback) errorcallback(err, response)};
            if(okcallback) okcallback(body, response);
        });
    }
}

module.exports = transactionservice;