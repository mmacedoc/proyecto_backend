var sgMail = require('@sendgrid/mail');
sgMail.setApiKey("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");
//sgMail.setApiKey("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");


var notificacionservice = {
    enviarEmailBienvenida: function(destino, nombre) {
        var msg = {
            to: destino,
            from: {
                email:'marco.macedoc@gmail.com',
                name: 'Tu Bankito',
            },
            templateId: 'd-d9c19bf2dde44804832d708911852cfa',
            dynamic_template_data: {
                nombre: nombre,
                action_url: 'http://localhost:8081/'
            }
          };
          //sgMail.send(msg);

          sgMail
            .send(msg)
            .then(() => {}, error => {
                console.error(error);
            
                if (error.response) {
                console.error(error.response.body)
                }
            });
    },
    enviarEmailResumen: function(destino, transaccion) {
        var msg = {
            to: destino,
            from: {
                email:'marco.macedoc@gmail.com',
                name: 'Tu Bankito',
            },
            templateId: 'd-193bf9f157244082ae10fd4b05725c76',
            dynamic_template_data: {
                nrooperacion: transaccion.identificador,
                ctaorigen: transaccion.origen.numero + transaccion.origen.identificador,
                ctadestino: transaccion.destino.numero + transaccion.destino.identificador,
                fecha: transaccion.fecha,
                monto: transaccion.moneda + " " + transaccion.monto,
                tipo_operacion: transaccion.tipooperacion
            }
          };
          //sgMail.send(msg);
          sgMail
            .send(msg)
            .then(() => {}, error => {
                console.error(error);
            
                if (error.response) {
                console.error(error.response.body)
                }
            });
    },
    enviarTokenTransaccion: function(destino, strtoken) {
        
        var msg = {
            to: destino,
            from: {
                email:'marco.macedoc@gmail.com',
                name: 'Tu Bankito',
            },
            templateId: 'd-2c1a3d7947d04de1b517a80824031530',
            dynamic_template_data: {
                token: strtoken
            }
          };
          //sgMail.send(msg);
          sgMail
            .send(msg)
            .then(() => {}, error => {
                console.error(error);            
                if (error.response) {
                console.error(error.response.body)
                }
            });
    }
}

module.exports = notificacionservice;